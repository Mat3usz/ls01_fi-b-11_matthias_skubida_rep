import java.util.Scanner;

public class Schleife2nachpruefend {
	

		public static void main(String[] args) {

			Scanner schleife = new Scanner (System.in);			// Scanner angelegt

			
			int n;
			
			System.out.println("Geben Sie bitte eine Zahl ein.");		// Aufgabe an den Nutzer, eine Zahl einzugeben
			
			n = schleife.nextInt();
			
			int i = -1;													// Zahl ist als 1 initialisiert
			
			do 
			{
				System.out.println(n);
				n--;
			}
			while (n >= i);												// While nach der geschweiften Klammer eingegeben
		}
		

	}

