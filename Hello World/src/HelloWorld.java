//
//public class HelloWorld {
//
//	public static void main(String[] args) {
//
//		System.out.println("Hello World");
//		System.out.println(" Guten Tag Matthias  "
//				+ "Willkommen in der Veranstaltung Strukturierte Programmierung.");
//		System.out.println("Hola");
//		
//	}
//
//}

public class Scanner {

	public static void main(String[] args) {
		
		Scanner myscanner = new Scanner(System.in);
	
		
		float zahl1, zahl2, summe;
		
		System.out.println("Das Programm addiert zwei Zahlen miteinander");
		System.out.println("bitte geben Sie die erste Zahl ein: ");
		zahl1 = myscanner.nextFloat();
		
		System.out.println("bitte geben Sie die zweite Zahl ein: ");
		zahl2 = myscanner.nextFloat();
		
		summe = zahl1 + zahl2;
		
		System.out.println("das ergebnis ist gleich: " +summe);
		
		System.out.println(summe);
		
		myscanner.close();

	}

}
