import java.util.Scanner;


public class Schleifen2 {

	public static void main(String[] args) {

		Scanner schleife = new Scanner (System.in);

		
		int n;
		
		System.out.println("Geben Sie bitte eine Zahl ein.");
		
		n = schleife.nextInt();
		
		int i = 1;
		
		while (  n >= i ) {
			System.out.println(n);
			n--;
			
		}
	}

}
