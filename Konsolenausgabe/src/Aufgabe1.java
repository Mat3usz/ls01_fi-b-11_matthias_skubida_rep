
public class Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		System.out.println("Hallo!\nWie geht es dir?");
		System.out.println("Hallo\b");
		System.out.println("Absatz\rNach unten");
		System.out.println("\t Horizontal Tab HAT");
		System.out.println("\"Crazy Stuff\"");
		
		String z = "Java-Programm";
		int n = 123;
		double k = 3.1452683;
		
		
		System.out.printf( "FI-B-11|%25s|", z );
		
		System.out.printf( "FI-B-11|%20.4s|", z );
		
		System.out.printf("%d" , n);
		System.out.printf("\n%d %d", n, n);	
		System.out.printf("\n%15d", n , n);
		System.out.printf("\n|%-15d||%15d|", n , n);
		System.out.printf("\n|%15d||%-15d|", n , n);
		System.out.printf("\n|%+-15d||%+15d|", n , n);
		System.out.printf("\n|%+-15d||%+15d|", -n , -n);
		System.out.printf("\n|%15d||%15d|", n , n);
		
		System.out.printf("\n|%f| ", k );
		System.out.printf("\n|%10f| |%10f|\n", k , k);

		
		System.out.printf("\n       * \n      *** \n     ***** \n    ******* \n   ********* \n  *********** \n ************* \n      *** \n      *** \n      ***");
		
		double l = 22.4234234;
		double o = 111.2222;
		double u = 4.0;
		double q = 1000000.551;
		double y = 97.34;
				
		System.out.printf("\n%.2f\n", l );
		System.out.printf("%.2f \n", o );
		System.out.printf("%.2f \n", u );
		System.out.printf("%.2f \n", q );
		System.out.printf("%.2f \n", y );
		
			 
				
	}

}
