import java.util.Scanner;


class FahrkartenautomatMitMethoden {
	public static void main(String[] args) {
		
		double zuzahlenderBetrag = fahrkartenBestellungErfassen();
		
		double rueckgabeBetrag = fahrkartenBezahlen(zuzahlenderBetrag);
		
		fahrscheinausgeben();
		
		rueckgeldAusgeben(rueckgabeBetrag);		
	}
		public static double fahrkartenBestellungErfassen()			//hier beginnt die erste Methode
		{																		
			Scanner ns = new Scanner(System.in);								
			System.out.print("Geben Sie den Einzelpreis pro Ticket ein: ");		
		    float preis = ns.nextFloat();										
		    System.out.print("Geben Sie die Anzahl der Tickets ein: ");
		    int anzahlTickets = ns.nextInt();
		    double zuZahlenderBetrag = preis*anzahlTickets;
		    return zuZahlenderBetrag;			
		}		
		public static double fahrkartenBezahlen(double zuzahlenderBetrag) 
		{
				float eingezahlterGesamtbetrag = 0.0f;
				float eingeworfeneMuenze;
				Scanner ns = new Scanner(System.in);
		       while(eingezahlterGesamtbetrag < zuzahlenderBetrag)
		       {
		    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuzahlenderBetrag - eingezahlterGesamtbetrag));
		    	   System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
		    	   eingeworfeneMuenze = ns.nextFloat();
		           eingezahlterGesamtbetrag += eingeworfeneMuenze;
		       }
		      
		       return eingezahlterGesamtbetrag - zuzahlenderBetrag;
		}
		public static void fahrscheinausgeben() 				//hier beginnt die zweite Methode.
		{
			 System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				  } catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				    }
		       }
		       System.out.println("\n\n");			
		}
		public static void rueckgeldAusgeben(double rueckgabeBetrag) //hier beginnt die dritte Methode.
		{
			if(rueckgabeBetrag > 0.0)
		       {
		    	   System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f EURO" , rueckgabeBetrag );
		    	   System.out.println("\nwird in folgenden Muenzen ausgezahlt:");
		           while(rueckgabeBetrag >= 2.0) // 2 EURO-Muenzen
		           {
		        	  System.out.println("2 EURO");
		        	  rueckgabeBetrag -= 2.0;
		           }
		           while(rueckgabeBetrag >= 1.0) // 1 EURO-Muenzen
		           {
		        	  System.out.println("1 EURO");
		        	  rueckgabeBetrag -= 1.0;
		           }
		           while(rueckgabeBetrag >= 0.5) // 50 CENT-Muenzen
		           {
		        	  System.out.println("50 CENT");
		        	  rueckgabeBetrag -= 0.5;
		           }
		           while(rueckgabeBetrag >= 0.2) // 20 CENT-Muenzen
		           {
		        	  System.out.println("20 CENT");
		        	  rueckgabeBetrag -= 0.2;
		           }
		           while(rueckgabeBetrag >= 0.1) // 10 CENT-Muenzen
		           {
		        	  System.out.println("10 CENT");
		        	  rueckgabeBetrag -= 0.1;
		           }
		           while(rueckgabeBetrag >= 0.05)// 5 CENT-Muenzen
		           {
		        	  System.out.println("5 CENT");
		        	  rueckgabeBetrag -= 0.05;
		           }
		       }
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wuenschen Ihnen eine gute Fahrt.");
		}
}
