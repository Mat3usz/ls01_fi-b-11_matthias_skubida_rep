
public class Temperaturtabelle_Skubida {

	public static void main(String[] args) {
		

		
		String F = "Fahrenheit"; // Namen in Variablen angegeben
		String C = "Celsius";
		
		double one = -20;
		double two = -28.8889;		// Zahlen ebenfalls in Variablen angegeben, zur besseren Unterscheidung
		double three = -10;
		double four = -23.3333;
		double five = 0;
		double six = -17.7778;
		double seven = 20;
		double eight = -6.6667;
		double nine = 30;
		double ten = -1.1111;
		
		
		System.out.printf("%-12s | %10s" , F , C );						// Ausgabe der Namen, mit Einhaltung der Zeilenbreite; links 12 und rechts 10
		System.out.printf("\n-------------------------");				// Ausgabe der "Minus"-Zeichen
		System.out.printf("\n%-12.2f | %10.2f" , one , two);			// Ausgabe der Zahlen, unter Einhaltung der 2 Nachkommastellen (.2f)
		System.out.printf("\n%-12.2f | %10.2f" , three , four);			
		System.out.printf("\n%+-12.2f | %10.2f" , five , six);			// Vor den positiven Fahrenheitsangaben ein "+" benutzt, damit das richtig ausgegeben wird
		System.out.printf("\n%+-12.2f | %10.2f" , seven , eight);
		System.out.printf("\n%+-12.2f | %10.2f" , nine , ten);
		
		
		
	}

}
