import java.util.Scanner;

public class Mittelwerterweitert {

	public static void main(String[] args) {
		
		double zahlwieviele;
		int zaehler = 0;																				// Z�hler
		double eingabezahl;
		
		double mittelwert = 0;																			// Mittelwert erstmal 0
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet den Mittelwert mehrerer Zahlen.");				//Text wie viele Zahlen?
		zahlwieviele = eingabe(myScanner, "Bitte geben Sie die gew�nschte Anzahl der Zahlen ein: ");
		
		do { 																					// nachpr�fende Schleife, da mindestens einmal ausgef�hrt werden muss
		
		eingabezahl = eingabe(myScanner, "Bitte geben Sie eine Zahl ein.");						// die einzugebenden Zahlen
		
		mittelwert = mittelwert + eingabezahl;																// Zahlen werden summiert
		
		zaehler++;
		}
		while (zahlwieviele > zaehler);															//Abbruchbedingung 
		
		mittelwert = mittelwert / zahlwieviele;
				
		ausgabe(mittelwert);																	// Ausgabe der unten definierten Methode
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text ) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}

	
	public static void ausgabe(double mittelwert) {												// Methode f�r den Mittelwert
		System.out.println("Der errechnete Mittelwert aller Zahlen ist: " + mittelwert);
	}
}
