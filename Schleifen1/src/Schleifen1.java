import java.util.Scanner;


public class Schleifen1 {

	public static void main(String[] args) {
		
		Scanner schleife = new Scanner (System.in);
		
		int n;
		
		
		System.out.println("Geben Sie bitte eine Zahl ein.");
		
		n = schleife.nextInt();
		
		
		for( int i = 1 ; n >= i ; n-- ) {			// Formel f�r 1,2,3,4,...n for( int i = 1 ; i <= n ; i++ )
			
			System.out.println(n);					// und hier muss dann (i) stehen
		}
		
	}

}
